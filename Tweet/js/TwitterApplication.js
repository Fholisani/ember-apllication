﻿App = Ember.Application.create();
App.ApplicationAdapter = DS.RESTAdapter;


App.User = DS.Model.extend({
    userName: DS.attr('string'),
    profilePictureUrl: DS.attr('string'),
    conversation: DS.belongsTo('conversation', { async: true }),
    followers: DS.hasMany('follower', { async: true }),
    tweets: DS.hasMany('tweet', { async: true }),
    //  responseTweets: DS.hasMany('responseTweet', { async: true })

});


App.Follower = DS.Model.extend({
    name: DS.attr('string'),
    user: DS.belongsTo('user')
});
App.Tweet = DS.Model.extend({
    tweetText: DS.attr(),
    lastTweetTime: DS.attr('date'),
    responseTweets: DS.hasMany('responseTweet', { async: true }),
    author: DS.belongsTo('user')
});

App.Conversation = DS.Model.extend({
    lastConversationTime: DS.attr('date'),
    user: DS.belongsTo('user'),
    tweets: DS.hasMany('tweet', { async: true })
});

App.ResponseTweet = DS.Model.extend({
    tweetText: DS.attr(),
    lastTweetTime: DS.attr('date'),
    responseTweets: DS.belongsTo('tweet'),
    author: DS.belongsTo('user')
});




App.Router.map(function () {
    // put your routes here
    this.resource('about');
    //render into the same tamplate url, child route
    this.resource('users', function () {
        this.resource('user', { path: ':user_id' });



    });
    ///tweets/1
    this.resource('conversations');

    this.resource('responseTweets');
    //this.resource('tweets');
    this.resource('tweets', function () {
        this.resource('tweet', { path: ':tweet_id' });

    });
});


App.AboutRoute = Ember.Route.extend();

//Route Telling templete which model is backed by
App.UsersRoute = Ember.Route.extend({

    model: function () {

        return this.store.find('user');
    }

});

//Incase of refresh, take parameter and load app with appropriate model data
App.UserRoute = Ember.Route.extend({
    model: function (params) {

        return this.store.find('user', params.user_id);
    }
    //afterModel: function (model, transition) {
    //    return Ember.RSVP.all(model.getEach('tweet'));
    //}

});

App.ConversationsRoute = Ember.Route.extend({
    model: function () {
        return this.store.find('conversation');
    }
});

App.ResponseTweetsRoute = Ember.Route.extend({
    model: function () {
        return this.store.find('responseTweet');
    }
});


App.TweetsRoute = Ember.Route.extend({
    model: function () {
        return this.store.find('tweet');
    }
});
//Incase of refresh, take parameter and load app with appropriate model data
App.TweetRoute = Ember.Route.extend({
    model: function (params) {

        return this.store.find('tweet', params.tweet_id);
    }

});

App.ResponseTweetRoute = Ember.Route.extend({
    model: function (params) {

        return this.store.find('responseTweet', params.responseTweet_id);
    }

});


//Keeping the state of the app, 
App.UserController = Ember.ObjectController.extend({
    isEditing: false,

    actions: {

        edit: function (tweet) {
            this.set('isEditing', true);
            var user = this.get('model');
            var tweets = user.get('tweets');

            this.set('isEditing', true);

            try {

                var tweetID = tweet.id;
             
                var response;
                var res;
                $.each(tweets.content.content, function (index, responseTweet) {
               
                    res = responseTweet._data.responseTweets;

                    $.each(res, function (i, responseTwitter) {
             
                        response = this.store.find('responseTweet', responseTwitter.id);
                  
                    });
               

                });


            } catch (e) {
                console.log(e);
            }



            // this.store.createRecord('Post', { title: this.get('title'), excerpt: this.get('excerpt'), body: this.get('body') });

            // This raise an error!


        },
        doneEditing: function () {
            this.set('isEditing', false);

        }
    }


});




$.mockjax({
    url: "/users",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        users: [{
            id: 1,
            userName: "Alan",
            profilePictureUrl: "/Content/Images/alan.png",
            conversation: 1,
            tweets: [1]
        },
        {
            id: 2,
            userName: "Martin",
            profilePictureUrl: "/Content/Images/Martin.png",
            conversation: 2,
            tweets: [3]
        },
        {
            id: 3,
            userName: "Ward",
            profilePictureUrl: "/Content/Images/profileTwitter.png",
            conversation: 1,
            tweets: [6]
        }]
    }

});

$.mockjax({
    url: "/conversations/1",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        conversation: {
            id: 1,
            user: 1,
            lastConversationTime: 'Sun Jan 07 2015 14:05:50',
            tweets: [1, 2, 3]
        }
    }
});
$.mockjax({
    url: "/conversations/2",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        conversation: {
            id: 2,
            user: 2,
            lastConversationTime: 'Sun Dec 31 2015 14:05:50',
            tweets: [1, 2, 3]
        }
    }
});



$.mockjax({
    url: "/tweets/1",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 1,
            tweetText: "Alan",
            lastTweetTime: 'Sun Dec 31 2013 14:05:50',
            responseTweets: [1, 2],
            author: 1
        }
    }
});


$.mockjax({
    url: "/tweets/2",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 2,
            tweetText: "Hello at Lunch",
            lastTweetTime: 'Sun Dec 31 2012 14:05:50',
            responseTweets: [2, 3],
            author: 1
        }
    }
});
$.mockjax({
    url: "/tweets/3",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 3,
            tweetText: "Martin",
            lastTweetTime: 'Sun Dec 31 2011 14:05:50',
            responseTweets: [],
            author: 2
        }
    }
});

$.mockjax({
    url: "/tweets/4",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 4,
            tweetText: "Hello at Work",
            lastTweetTime: 'Sun Dec 31 2010 14:05:50',
            responseTweets: [2, 3],
            author: 5
        }
    }
});
$.mockjax({
    url: "/tweets/5",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 5,
            tweetText: "Hello at School",
            lastTweetTime: 'Sun Dec 31 2009 14:05:50',
            responseTweets: [4, 5],
            author: 5
        }
    }
});



$.mockjax({
    url: "/tweets/6",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 6,
            tweetText: "Ward",
            lastTweetTime: 'Sun Dec 31 2008 14:05:50',
            responseTweets: [4,3,5],
            author: 3
        }
    }
});
$.mockjax({
    url: "/tweets/7",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 7,
            tweetText: "Hello at School",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets: [4, 5],
            author: 4
        }
    }
});

$.mockjax({
    url: "/tweets/7",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        tweet: {
            id: 7,
            tweetText: "Hello at School",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets: [4, 5],
            author: 4
        }
    }
});


$.mockjax({
    url: "/responseTweets/1",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        responseTweet: {
            id: 1,
            tweetText: "If you have a procedure with 10 parameters, you probably missed some",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets:1,
            author: 1
        }
    }
});

$.mockjax({
    url: "/responseTweets/2",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        responseTweet: {
            id: 2,
            tweetText: "Random numbers should not be generated with a method chosen at random.",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets:1,
            author: 1
        }
    }
});

$.mockjax({
    url: "/responseTweets/3",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        responseTweet: {
            id: 3,
            tweetText: "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets:6,
            author: 3
        }
    }
});


$.mockjax({
    url: "/responseTweets/4",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        responseTweet: {
            id: 4,
            tweetText: "If you have a procedure with 10 parameters, you probably missed some",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets: 6,
            author: 1
        }
    }
});

$.mockjax({
    url: "/responseTweets/5",
    type: "GET",
    status: 200,
    statusText: "OK",
    responseText: {
        responseTweet: {
            id: 5,
            tweetText: "Random numbers should not be generated with a method chosen at random.",
            lastTweetTime: 'Sun Dec 31 2007 14:05:50',
            responseTweets: 6,
            author: 1
        }
    }
});





//Help format specific value, takes date as input
Ember.Handlebars.helper('format-date', function (date) {
    return moment(date).fromNow();
});

var showdown = new Showdown.converter

Ember.Handlebars.helper('format-markdown', function (input) {
    return new Handlebars.SafeString(showdown.makeHtml(input));
});
